// Теоретичні питання:
// 1. Метод forEach перебирає кожний елемент масиву та запускає задану callback функцію для кожного з них;
// 2. Очищаємо масив: arr.length = 0; або arr.splice(0,arr.length);
// 3. В цьому нам допоможе метод arr.isArray().

// Завдання:

function filterBy(arr, dataType) {
  return arr.filter((value) => typeof(value) !== dataType);
}
// Перевіряємо, працює):
console.log(filterBy(['hello', 'world', 23, '23', null], 'string'));
